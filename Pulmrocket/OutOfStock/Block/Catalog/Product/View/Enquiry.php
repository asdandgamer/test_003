<?php
 
namespace Pulmrocket\OutOfStock\Block\Catalog\Product\View;
 
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\Registry;
use Pulmrocket\OutOfStock\Helper\Data;
 
class Enquiry extends AbstractProduct
{
    protected $_stockItemRepository;
    protected $_registry;
    protected $_helperData;

    public function __construct(
        Context $context, 
        array $data, 
        StockItemRepository $stockItemRepository, 
        Registry $registry,
        Data $helperData)
    {
        $this->_registry = $registry;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_helperData = $helperData;
        parent::__construct($context, $data);
    }
    public function isInStock()
    {
        $currentProduct = $this->_registry->registry('current_product');
        $_productStock = $this->_stockItemRepository->get($currentProduct->getId());
        return $_productStock->getIsInStock();
    }
    public function isModuleEnabled()
    {
        return $this->_helperData->isModuleEnabled();
    }
    public function getCurrentProductID() {
        return $this->_registry->registry('current_product')->getId();
    }
    public function getCurProdUrl() {
        return $this->_registry->registry('current_product')->getProductUrl();
    }
}