<?php 
/*
    Links: 
    get product price: [ https://magento.stackexchange.com/questions/257587/magento-2-how-to-get-final-price-original-price-of-all-types-of-product ]

*/
namespace Pulmrocket\OutOfStock\Block\Customer;

use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\Session;

// use Pulmrocket\OutOfStock\Helper\Data;
// use Pulmrocket\OutOfStock\Model\Products;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productRepository;
    protected $_session;
    // protected $_helperData;
    // protected $productsModel;
		
    public function __construct(
        Context $context, 
        ProductRepository $productRepository,
        Session $session,
        // Products $productsModel,
        // Data $helperData,

        array $data = []
	)
	{
        // $objectManager = $this->_objectManager;
        // $this->_productRepository = $productRepository;
        // $this->productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Helper\Data::class);
        // $this->productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Products::class);
        // $objectManager->configure($configLoader->load('frontend'));
        
        // $this->_helperData = $helperData;
        $this->_productRepository = $productRepository;
        parent::__construct($context, $data);
        $this->_session = $session;
        // $this->productsModel = $productsModel;
    }
    
    // public function _prepareLayout()
    // {
    //     var_dump($this->_session->getCustomer()->getEmail());
    //     // exit();
    //     return parent::_prepareLayout();
    // }
    // public function getSubscriberSubs()
    // {
    //     // $productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Products::class);
    //     // $subs=$this->productsModel->getSubsByMail($this->getCustomerEmail());
    //     // return $subs;
    //     return 'sdfadsfa';
    // }

    function getProductPrice($product) {
        $regularPrice = 0; $specialPrice = 0;
        switch($product->getTypeId()) {
            case 'simple': 
                $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getValue();
                $specialPrice = $product->getPriceInfo()->getPrice('special_price')->getValue();
                break;
            case 'configurable': 
                $basePrice = $product->getPriceInfo()->getPrice('regular_price');
    
                $regularPrice = $basePrice->getMinRegularAmount()->getValue();
                $specialPrice = $product->getFinalPrice();
                break;
            case 'grouped': 
                $usedProds = $product->getTypeInstance(true)->getAssociatedProducts($product);            
                foreach ($usedProds as $child) {
                    if ($child->getId() != $product->getId()) {
                            $regularPrice += $child->getPrice();
                            $specialPrice += $child->getFinalPrice();
                    }
                }
                break;
            case 'bundle': 
                $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getMinimalPrice()->getValue();
                $specialPrice = $product->getPriceInfo()->getPrice('final_price')->getMinimalPrice()->getValue();
                break;
            // case 'virtual': break;
            // case 'downloadable': break;
            default: return 0;
        }
        if($regularPrice>$specialPrice) return $specialPrice;
        else return $regularPrice;
    }
	
	public function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}
	public function getProductBySku($sku)
	{
		return $this->_productRepository->get($sku);
    }
    public function getCustomerEmail() {
        return $this->_session->getCustomer()->getEmail();
    }
    // public function isModuleEnabled()
    // {
    //     return $this->_helperData->isModuleEnabled();
    // }
}