<?php
namespace Pulmrocket\OutOfStock\Controller\Adminhtml\Oos;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\UrlInterface;

class Delete extends \Magento\Framework\App\Action\Action
{
    protected $_request;
    private $urlBuilder;

    const URL_PATH_OOS = 'pulmrocket_oos/oos/';

    public function __construct(Context $context, Http $request,  UrlInterface $urlBuilder) 
    {
        $this->_request = $request;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context);
    }
    public function execute()
	{
        $url = $this->urlBuilder->getUrl(self::URL_PATH_OOS);

        $productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Products::class);
        $prod_id = $this->_request->getParams()["prod_id"];
        $productsModel->DeleteSubs($prod_id);
        // echo '$prod_id =' . $prod_id;
        echo '<pre>';
        var_dump($this->_request->getParams());
        var_dump($url);
        echo '</pre>';
        
        header('Location: ' . $url, true, 303);
        exit;
    }
}
// http://127.0.0.1/admin/pulmrocket_oos/oos/delete/product_id/12