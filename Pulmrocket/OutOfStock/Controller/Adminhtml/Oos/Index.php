<?php

namespace Pulmrocket\OutOfStock\Controller\Adminhtml\Oos;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;

	public function __construct(Context $context, PageFactory $resultPageFactory)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Subs')));

		return $resultPage;
	}
}

/* NOTE: to disable admin form key exec bash command:
	bin/magento config:set admin/security/use_form_key 0 */