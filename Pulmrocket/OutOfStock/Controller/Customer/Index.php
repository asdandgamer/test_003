<?php 
namespace Pulmrocket\OutOfStock\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_session;

	public function __construct(Context $context, PageFactory $pageFactory, Session $session)
	{
        $this->_session = $session;
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        if (!$this->_session->isLoggedIn())
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login');
            return $resultRedirect;
        }
        else
        {
            /* $page = $this->_pageFactory->create();
            // $page->addHandle('oos_customer_index');
            echo '<pre>'; var_dump($page->getLayout()->getUpdate()->getHandles()); echo '</pre>';
            return $page; */
            return $this->_pageFactory->create();
        }
    }
}

// bin/magento setup:di:compile // If layout won't loads