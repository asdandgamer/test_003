<?php
namespace Pulmrocket\OutOfStock\Controller\Index;
use Pulmrocket\OutOfStock\Model\Mails;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Request\Http;
// use Pulmrocket\OutOfStock\Helper\Data;

class Test extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	protected $helperData;
	protected $_request;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Pulmrocket\OutOfStock\Helper\Data $helperData,
		Http $request)
	{
		$this->_pageFactory = $pageFactory;
		$this->helperData = $helperData;
		$this->_request = $request;
		return parent::__construct($context);
	}

	public function execute()
	{
		/*
		$a = new A;
		$b = new B;

		echo $a->getClassName();      // A
		echo $a->getRealClassName();  // A
		echo $b->getClassName();      // A
		echo $b->getRealClassName();  // B
		*/
		// $helperData = new \Pulmrocket\OutOfStock\Helper\Data;
		echo $this->helperData->getGeneralConfig('enable'); // is module enabled

		// $this->scopeConfig->getValue('oos/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

		echo '<pre>';
		// $contactModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Mails::class);
		// $collection = $contactModel->getCollection();
        // foreach($collection as $contact) {
        //     var_dump($contact->getData());
		// }
		// echo '</br>';
		$productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Products::class);
		$prods = $productsModel->getProductSubsById('700');
		//foreach($prods as $prod) {
			var_dump($prods);
		// }
		//https://www.pierrefay.com/magento2-training/create-magento2-model-database.html
		echo '</pre>';
		echo '<pre>';
        var_dump($this->_request->getParams());
        echo '</pre>';
		exit;
	}
}

class A {

    public function getClassName(){
        return __CLASS__;
    }

    public function getRealClassName() {
        return static::class;
    }
}

class B extends A {}

