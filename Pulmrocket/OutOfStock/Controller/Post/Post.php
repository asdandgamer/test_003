<?php
namespace Pulmrocket\OutOfStock\Controller\Post;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Pulmrocket\OutOfStock\Model\Products;

class Post extends \Magento\Framework\App\Action\Action
{
    protected $_resultJsonFactory;
    protected $productsModel;

    public function __construct(Context $context, JsonFactory $resultJsonFactory, Products $productsModel)
	{
        $this->productsModel = $productsModel;
        $this->_resultJsonFactory = $resultJsonFactory;
		return parent::__construct($context);
    }
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $post = $this->getRequest()->getPost();
        // $productsModel = $this->_objectManager->create(\Pulmrocket\OutOfStock\Model\Products::class);
        //-----------------------------------------------------------
        if ($this->getRequest()->isAjax()) 
        {
            if(!filter_var($post['mail'], FILTER_VALIDATE_EMAIL))
                return $result->setData(array('status' => false, 'msg' => 'Invalid e-mail!'));
            $res = $this->productsModel->Subscribe($post['mail'], $post['productID']);
            return $result->setData(array('status' => $res));
        }
    }
}