<?php
namespace Pulmrocket\OutOfStock\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

class Products extends AbstractModel
{
    protected $_productRepository;

    protected function _construct()
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_productRepository = $_objectManager->create(\Magento\Catalog\Model\ProductRepository::class);
        $this->_init(\Pulmrocket\OutOfStock\Model\ResourceModel\Products::class);
    }
    public function Subscribe($mail, $prodID) 
    {
        $prod = $this->getCollection()->addFieldToFilter('product_id', $prodID);
        // check records count [ if more than 1 time > error]
        if($prod->count() > 1) return false; else if($prod->count() == 1) $isprod=true; else $isprod=false;
        // get record row to complete
        if($isprod) { // product already exist
            $prod_field = $this->queryToArray($prod)[0];
            // return $prod_field[0]['email_ids'];
            $mails = explode(',', $prod_field['email_ids']);
            if(array_search($mail, $mails)) return true; // mail is already subscribed
            else {
                array_push($mails, $mail);
                $sub_count = count($mails);
                $mails = implode(',', $mails);
                $prod->getFirstItem()->setData('email_ids',$mails)->save();
                $prod->getFirstItem()->setData('sub_count',$sub_count)->save();
            }
        } else {
            $product = $this->getProductById($prodID);
            $this->addData([
                "product_name" => $product->getName(),
                "product_sku" => $product->getSku(),
                "sub_count" => 1,
                "product_id" => $prodID,
                "email_ids" => $mail
                ])->save();
        }
        return true;
    }
    public function DeleteSubs($prod_id) {
        $prod = $this->getCollection()->addFieldToFilter('prod_id', $prod_id);
        $prod->getFirstItem()->delete();
    }
    public function getSubsByMail($mail) {
        // $collection = $this->getCollection()->addFieldToFilter('email_ids',// array(
        //     array('like' => '% '.$mail.' %'), //spaces on each side
        //     array('like' => '% '.$mail), //space before and ends with $needle
        //     array('like' => $mail.' %') // starts with needle and space after
        // ));
        $collection = $this->getCollection()->addFieldToFilter('email_ids', ['like' => '%'.$mail.'%']);
        return $this->queryToArray($collection);
    }
    public function getProductSubsById($prod_id) {
        $prod = $this->getCollection()->addFieldToFilter('product_id', $prod_id);
        if($prod->count() == 1) {
            $prod_field = $this->queryToArray($prod)[0];
            return explode(',', $prod_field['email_ids']);
        } else 
            return false;
    }
    private function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}
    private function queryToArray($query) 
    {
        $arr = array();
        foreach($query as $q) {
			array_push($arr, $q->getData());
        }
        return $arr;
    }
}