<?php

namespace Pulmrocket\OutOfStock\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Products extends AbstractDb
{
    public function _construct()
    {
        $this->_init('pulmrocket_oos_products', 'prod_id');
    }
}

/* INSERT INTO pulmrocket_oos_products (product_name, sku, sub_count, email_ids) VALUES ('product 1', 3770, 1, '1,'); */