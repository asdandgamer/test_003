<?php

namespace Pulmrocket\OutOfStock\Model\ResourceModel\Products;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(\Pulmrocket\OutOfStock\Model\Products::class, \Pulmrocket\OutOfStock\Model\ResourceModel\Products::class);
    }
}