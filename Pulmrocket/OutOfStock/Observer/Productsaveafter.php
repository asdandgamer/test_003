<?php 
/* 
Links: 
    https://magento.stackexchange.com/questions/276684/magento-2-event-observer-for-whenever-the-stock-status-of-product-changes
    https://www.mageplaza.com/magento-2-module-development/magento-2-create-events.html
*/
namespace Pulmrocket\OutOfStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Pulmrocket\OutOfStock\Helper\Email;
use Pulmrocket\OutOfStock\Model\Products;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;

class Productsaveafter implements ObserverInterface
{
    private $helperEmail;
    private $productsModel;
    private $_stockItemRepository;
    
    public function __construct(
        Email $helperEmail,
        Products $productsModel,
        StockItemRepository $stockItemRepository
    ) {
        $this->helperEmail = $helperEmail;
        $this->productsModel = $productsModel;
        $this->_stockItemRepository = $stockItemRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $_product = $observer->getProduct(); // you will get product object and you can check for stock and attribute values here
        $isInStock = $this->_stockItemRepository->get($_product->getId())->getIsInStock();
        if($isInStock) {
            $recipients = $this->productsModel->getProductSubsById($_product->getId());
            if(!$recipients) return false;
            foreach ($recipients as $recipient) {
                $this->helperEmail->sendEmail($recipient);
            }
        } else return;
    }   
}