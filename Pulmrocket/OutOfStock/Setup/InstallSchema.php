<?php 
namespace Pulmrocket\OutOfStock\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('pulmrocket_oos_products')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('pulmrocket_oos_products')
            ) -> addColumn(
                'prod_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true
                ],
                'Record ID'
            ) -> addColumn(
                'website',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'default' => 'Main Website'
                ],
                'Website'
            ) -> addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                ],
                'Product ID'
            ) -> addColumn(
                'product_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Product name'
            ) -> addColumn(
                'product_sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'default' => '0'
                ],
                'SKU field'
            ) -> addColumn(
                'first_sub',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'First subscription'
            ) -> addColumn(
                'last_sub',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                ],
                'Last Subscription'
            ) -> addColumn(
                'sub_count',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                ],
                'Customer Awaiting Notification'
            ) -> addColumn(
                'email_ids',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [
                    'nullable' => false,
                ],
                'e-mail ID\'s'
            )->setComment('Products subs Table');
			$installer->getConnection()->createTable($table);;
        }
        $installer->endSetup();
    }
}
