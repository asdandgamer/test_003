<?php
namespace Pulmrocket\OutOfStock\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;
		$installer->startSetup();
		if(version_compare($context->getVersion(), '0.0.4', '<')) {
            $installer->getConnection()->dropColumn($installer->getTable('pulmrocket_oos_products'), 'email_ids');
			$installer->getConnection()->addColumn(
				$installer->getTable( 'pulmrocket_oos_products' ),
                'email_ids',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' =>'64k',
                    'nullable' => false,
                    'comment' => 'e-mails'
                ]
            );
		}
		$installer->endSetup();
	}
}
